# Pizza Fork

Le projet pizza fork est un projet codeIgniter4 qui consiste à un site répertoriant des pizzas.

## Installation du projet

tout d'abord après avoir récupéré mon projet je dois lancer la commande suivante.

```shell
composer update
```

une fois cela fait je dois créer ma bdd avec la commande suivante.

```shell
php spark db:create
```

*Une fois cela fait je dois donner un nom a ma bdd*

![bddCréer](imgReadme/dbCreate.png)

Après cela, je vais lancer la commande suivante afin de lancer les migrations de ma base de données qui me permettras de créer les tables de ma bdd.

```shell
php spark migrate
```

*Voici un screenshot du shell*

![bddMigrations](imgReadme/sparkMigrate.png)

Avant de d'alimenter ma bdd avec des données, je vais devoir dupliquer mon fichier *env*, puis. Je vais nommer le nouveau fichier *.env* et devoir spécifier mes infos de bdd telle que présenté dans cette image.

![envBdd](imgReadme/imgEnv.png)

**Attention** si l'accés au compte root ne nécessite pas de mot de passe, le dièse peut être garder devant la ligne correspondant.

Pour la suite, il faut utiliser la commande suivante qui permet d'alimenter les tables de nôtre bdd.

```shell
php spark db:seed
```

Notre projet à trois seeders, c'est pourquoi nous rentrerons la commande trois fois, tout en spécifiant le nom des seeders dans cette ordre.

```shell
PizzaSeeder
```

```shell
IngredientSeeder
```

```shell
GarnitureSeeder
```

Pour finir, nous devons créer le dossier **img** dans **/public/img/**. C'est un dossier non récupéré de par les paramètres de nôtre **.gitignore**

## Les pizzas

Voici un aperçu de nôtre page pizza.

![pagePizza](imgReadme/pizzaIndex.png)

Nous avons ici plusieurs actions sur cette page
, Modifier une pizza **(Bouton bleue)**
, Modifier les ingrédients de la pizza **(Bouton vert)**
, Supprimer une pizza **(Bouton rouge)**.

La page modif de pizza, permet comme son nom l'indique de modifier le nom d'une pizza et son image, et elle se présente comme ceci.

![modifDePizza](imgReadme/editPizza.png)

La page de modif d'ingredient permet de rajouter des ingredients a une pizza en choissisant la quantité d'ingredient et leur ordre, ainsi que de supprimer des ingrédients.

![ingredientDePizza](imgReadme/editIngredientOfPizza.png)

La modification d'une garniture de pizza ce présente comme ceci.

![modifGarnitureOfPizza](imgReadme/editGarnitureOfPizza.png)

*Le bouton rouge permet de supprimer une garniture de nôtre pizza*

Pour rajouter une garniture a nôtre pizza le bouton + en bas de nôtre page permet cela, voila ce que donne la page ajout d'une garniture.

![ajoutGarnitureOfPizza](imgReadme/ajoutGarnitureOfPizza.png)

Enfin pour rajouter une pizza, en bas de nôtre page Pizza-Index ce trouve un bouton + en dessous de nôtre navigation bar, voici l'aperçu de la page ajout d'une pizza.

![ajoutPizza](imgReadme/ajoutPizza.png)

**Attention** lors de la création de la pizza une pâte a pain est rajouté automatiquement et celle-ci ne peut pas être supprimé a moins de supprimer totalement la pizza.

## Les ingredients

voici maintenant la page des ingredients.

![pageIngredients](imgReadme/ingredientsPage.png)

Comme sur la page Pizza, le bouton bleue permet de modifier un ingredient et le rouge de supprimer un ingredient.

**Attention** *un ingredient utilisé par plusieurs pizzas ne peut pas être supprimé. Voici la page de modif d'ingredient*.

![modifIngredient](imgReadme/modifIngredient.png)

En bas de la page, nous pouvons comme la page pizza ajouté un ingredient.

![ajoutIngredient](imgReadme/ajoutIngredient.png)

## Le panier

Le panier permet, de commander des pizzas, de vérifier le contenu du panier, d'incrémenter une pizza du panier et de la décrémenter, et de pouvoir supprimer une pizza du panier.

Voici le shéma de cas d'utilisation de nôtre panier.

```plantuml
:Client:
package Panier{
    Client--->(Liste des pizzas à commander)
    Client--->(Ajouter une pizza au panier)
    Client--->(Vérification du contenu du panier)
    Client--->(Incrémenter une pizza du panier)
    Client--->(Décrémenter une pizza du panier)
    Client--->(Supprimer une pizza du panier)
}
```

Maintenant nous allons voir nôtre page index, la page de commande de pizza.

![indexPage](imgReadme/commandPizza.png)

En bas de la page près de nôtre nav bar ce trouve le bouton checkout qui nous permet de vérifier le contenu de nôtre panier.

![boutonPanier](imgReadme/commandPizzaCheckout.png)

Et après avoir commandé des pizzas et cliqué sur le bouton checkout voici nôtre panier.

![panier](imgReadme/panier.png)