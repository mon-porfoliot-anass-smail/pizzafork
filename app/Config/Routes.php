<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('HomeController');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'HomeController::index');
//
$routes->get('/bag/add/(:num)', 'BagController::add/$1');
$routes->get('/bag/checkout/', 'BagController::index');
$routes->get('/bag/qty/Dec/(:alphanum)','BagController::DeIncrementQty/$1');
$routes->get('/bag/qty/Inc/(:alphanum)','BagController::IncrementQty/$1');
$routes->get('/bag/item/delete/(:alphanum)', 'BagController::delete/$1');
//
$routes->get('/pizzas', 'PizzaController::index');
$routes->get('/pizza/create', 'PizzaController::create');
$routes->get('/pizza/delete/(:num)', 'PizzaController::delete/$1');
$routes->post('/pizza/save', 'PizzaController::save');
$routes->get('/pizza/edit/(:num)', 'PizzaController::edit/$1');
$routes->post('/pizza/save/(:num)', 'PizzaController::save/$1');
//
$routes->get('/ingredients', 'IngredientController::index');
$routes->get('/ingredient/create', 'IngredientController::create');
$routes->get('/ingredient/delete/(:num)', 'IngredientController::delete/$1');
$routes->post('/ingredient/save', 'IngredientController::save');
$routes->get('/ingredient/edit/(:num)', 'IngredientController::edit/$1');
$routes->post('/ingredient/save/(:num)', 'IngredientController::save/$1');
//
$routes->get('/pizza/ingredients/(:num)', 'GarnitureController::index/$1');
$routes->get('/pizza/ingredient/create/(:num)', 'GarnitureController::create/$1');
$routes->get('/pizza/ingredient/delete/(:num)', 'GarnitureController::delete/$1');
$routes->post('/pizza/ingredient/save', 'GarnitureController::save');
$routes->get('/pizza/ingredient/edit/(:num)', 'GarnitureController::edit/$1');
$routes->post('/pizza/ingredient/save/(:num)', 'GarnitureController::save/$1');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
