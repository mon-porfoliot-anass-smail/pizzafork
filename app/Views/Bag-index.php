<?= $this->extend('page.php') ?>
<?= $this->section('body') ?>
<div class="card">
    <div class="card-header">
        <div class="col">
            <h4><?= $title ?><?= '(&euro;'.$TotalPrice.')'?></h4>
        </div>
    </div>
</div>
<div class="card-body">
    <thead>
    </thead>
    <table class="table table-hover table-striped">
        <?php foreach ($bag->contents() as $item): ?>
            <tr style="text-align: center;">
                <td>
                    <img src="<?= '/img/' . $item['picture'] ?>" width="60" height="40" class="img-fluid rounded" alt="...">
                </td>
                <td>
                    <h5>
                        <?= $item['name'] ?>
                    <h5>
                </td>
                <td style="width: 5%;">
                    <a href="<?= '/bag/qty/Dec/'.$item['rowid'] ?>" class="btn btn-danger" role="button">
                        <i class="fas fa-minus"></i>
                    </a>
                </td>
                <td style="width: 10%;">
                    <h5>
                        <?= $bag->formatNumber($item['qty']) ?>
                    </h5>
                </td>
                <td style="width: 5%;">
                    <a href="<?= '/bag/qty/Inc/'.$item['rowid'] ?>" class="btn btn-success" role="button">
                        <i class="fas fa-plus"></i>
                    </a>
                </td>
                <td>
                    <h5 class="text-grey">
                        <?= '&euro;' . $bag->formatNumber($item['price']) ?>
                    </h5>
                </td>
                <td>
                    <a href="<?= '/bag/item/delete/'.$item['rowid'] ?>" class="btn btn-danger" role="button">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach ?>
    </table>
</div>
<?= $this->endSection() ?>