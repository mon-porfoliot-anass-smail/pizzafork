<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\PizzaModel;

class BagController extends BaseController {
    public function index() {
        $data['TotalPrice'] = 0;
        $data['title']= 'Votre panier';
        $data['bag'] = \Config\Services::cart();
        $bag = \Config\Services::cart();
        foreach($bag->contents() as $pizza){
            $data['TotalPrice']= $data['TotalPrice']+ $pizza['qty'] * $pizza['price'];
        }
        return view('Bag-index.php', $data);
    }

    public function add($idPizza) {
        $pizzaModel = new PizzaModel();
        $pizza = $pizzaModel->find($idPizza);
        $bag = \Config\Services::cart();
        $bag->insert(
            array(
                'id' => $idPizza,
                'qty' => 1, 
                'price' => '10.50',
                'name' => $pizza->text,
                'picture' => $pizza->picture,
            )
        );
        $quantite = 0;
        foreach($bag->contents() as $pizza) {
            if($idPizza == $pizza['id']){
                $quantite = $quantite + $pizza['qty'];
                break;
            }
        }
        return redirect()->to('/')->with('message', 'Ajout de la pizza '.$pizza['name'].' dans le panier. Quantité: '.$quantite);
    }

    public function delete($bagRowId) {
        $bag = \Config\Services::cart();
        $Pizza = $bag->getItem($bagRowId);
        $bag->remove($bagRowId);
        return redirect()->to('/bag/checkout')->with('message', 'Suppression de la pizza '.$Pizza['name'].' du panier.');
    }

    public function IncrementQty($bagRowId) {
        $bag = \Config\Services::cart();
        $Pizza = $bag->getItem($bagRowId);
        $Pizza['qty'] = $Pizza['qty'] + 1;
        $bag->update($Pizza);
        return redirect()->to('/bag/checkout')->with('message', 'Augmentation de la quantité de la pizza '.$Pizza['name'].'.');
    }

    public function DeIncrementQty($bagRowId) {
        $bag = \Config\Services::cart();
        $Pizza = $bag->getItem($bagRowId);
        if($Pizza['qty'] < 1 || $Pizza['qty'] == 1){
            return redirect()->to('/bag/checkout')->with('error', 'La quantité de la pizza '.$Pizza['name'].' ne peut pas être inférieur à 1');
        }else{
            $Pizza['qty'] = $Pizza['qty'] - 1;
            $bag->update($Pizza);
            return redirect()->to('/bag/checkout')->with('message', 'Diminution de la quantité de la pizza '.$Pizza['name'].'.');
        }
    }
}
